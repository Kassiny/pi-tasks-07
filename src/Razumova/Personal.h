#pragma once


#include "Employee.h"
#include "Worktime.h"

class  Personal:public Employee, public Worktime
{
public:
	Personal();
	Personal(int id, std::string name, double rate);
	void calc() override;
	double premiumhour() override;
protected:
	int worktime;
	double rate;
};

class Cleaner :public Personal
{
public:
	Cleaner();
	Cleaner(int id, std::string name);
	void doWork() override;
	void doWork(int days) override;
};

class Driver :public Personal
{
public:
	Driver();
	Driver(int id, std::string name);
	void doWork() override;
	void doWork(int days) override;
};