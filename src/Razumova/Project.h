#pragma once
#include <string>

class Project
{
public:
	virtual double premiumProject() = 0;
};


struct Proj
{
	std::string name;
	double budget;
	int programmers;
	int testers;
	int managers;
	Proj()
	{
		programmers = 0;
		testers = 0;
		managers = 0;
	}
};