#include "Management.h"

using namespace std;

Manager::Manager():Employee(),project(NULL),rateProject(0.7)
{
}
Manager::Manager(int id, std::string name):Employee(id, name),rateProject(0.7),project(NULL)
{
}
double Manager::premiumProject()
{
	if (project == NULL)
		return 0.0;
	return project->budget*rateProject;
}

void Manager::calc()
{
	payment += premiumProject();
}

void Manager::setProject(Proj * p)
{
	project = p;
	project->managers++;
}

ProjectManager::ProjectManager():Manager()
{
	rateProject = 0.1;

}

ProjectManager::ProjectManager(int id, std::string name):Manager(id,name)
{
	rateProject = 0.1;

}

double ProjectManager::premiumHeading()
{
	if (project == NULL)
		return 0.0;
	return project->managers * 500;
}

void ProjectManager::calc()
{
	this->Manager::calc();
	payment += premiumHeading();
}

SeniorManager::SeniorManager() :ProjectManager()
{
}

SeniorManager::SeniorManager(int id, std::string name):ProjectManager(id, name)
{

}

void SeniorManager::calc()
{
	ProjectManager::calc();
	payment += 1500 * sub;
	for (int i = 0; i < projects.size(); i++)
	{
		payment += projects[i].budget*0.05;
	}
}

void SeniorManager::setSubs(int n)
{
	sub = n;
}

void SeniorManager::setProjects(vector<Proj>& p)
{
	projects = p;
}
