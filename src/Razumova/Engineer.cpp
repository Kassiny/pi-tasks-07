#include "Engineer.h"

Engineer::Engineer():Employee(),rateHour(0),rateProject(0),worktime(0), projectName(""), project(NULL)
{}

Engineer::Engineer(int id, string name):Employee(id,name),worktime(0),\
rateHour(0), rateProject(0), projectName(""), project(NULL)
{}
Engineer::Engineer(int id, string name, double rateH, double rateP): Employee(id, name),worktime(0),\
rateHour(rateH),rateProject(rateP), project(NULL)
{}

double Engineer::premiumhour()
{
	return worktime*rateHour;
}

double Engineer::premiumProject()
{
	return rateProject*project->budget;
}

void Engineer::calc()
{
	payment += (premiumhour() + premiumProject());
	worktime = 0;
}

void Engineer::setProject(Proj *p)
{
	project = p;
}

Programmer::Programmer() :Engineer()
{}
Programmer::Programmer(int id, string name) : Engineer(id, name, 70, 0.1)
{}

void Programmer::doWork()
{
	int h = 8;
	worktime += h;
}
void Programmer::doWork(int days)
{
	int h = 8;
	worktime += h*days;
}

void Programmer::setProject(Proj * p)
{
	project = p;
	project->programmers++;
}

Tester::Tester() :Engineer() {}

Tester:: Tester(int id, string name):Engineer(id,name,70,0.07)
{}

void Tester::doWork()
{
	int h = 6;
	worktime += h;
}

void Tester:: doWork(int days)
{
	int h = 8;
	worktime += h*days;
}

void Tester::setProject(Proj * p)
{
	project = p;
	project->testers++;
}

TeamLeader::TeamLeader(): Programmer()
{
}

TeamLeader::TeamLeader(int id, string name):Programmer(id,name)
{}

double TeamLeader::premiumHeading()
{
	if (project == NULL)
		return 0.0;
	return project->programmers * 1000 + project->testers * 500;
}


void TeamLeader::calc()
{
	this->Programmer::calc();
	payment += premiumHeading();
}
