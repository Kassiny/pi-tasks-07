#include "Personal.h"

using namespace std;

double Personal::premiumhour()
{
	return worktime*rate;
}

void Personal::calc()
{
	payment += premiumhour();
	worktime = 0;
}

 Personal::Personal():Employee::Employee(),worktime(0)
{}

Personal::Personal(int id, string name, double rate) : Employee::Employee(id, name), worktime(0)
{
	this->rate = rate;
}

Cleaner::Cleaner():Personal::Personal()
{}

Cleaner::Cleaner(int id, string name):Personal::Personal(id,name,100.0)
{}

void Cleaner::doWork()
{
	int h = 8;
	worktime += h;
}
void Cleaner::doWork(int days)
{
	int h = 8;
	worktime += h*days;
}



Driver::Driver() :Personal::Personal()
{}

Driver::Driver(int id, string name) : Personal::Personal(id, name, 200.0)
{}

void Driver::doWork()
{
	int h = 6;
	worktime += h;
}

void Driver::doWork(int days)
{
	int h = 6;
	worktime += h*days;
}
