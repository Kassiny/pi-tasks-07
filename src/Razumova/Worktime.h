#pragma once

class Worktime
{
public:
	virtual double premiumhour() = 0;
	virtual void doWork() = 0;
	virtual void doWork(int days)=0;
};