#pragma once

#include "Employee.h"
#include "Project.h"
#include "Heading.h"
#include <vector>


class Manager :public Employee, public Project
{
public:
	Manager();
	Manager(int id, std::string name);
	double premiumProject() override;
	void calc() override;
	void setProject(Proj *p);
protected:
	double rateProject;
	Proj *project;
};

class ProjectManager :public Manager, public Heading
{
public:
	ProjectManager();
	ProjectManager(int id, std::string name);
	double premiumHeading() override;
	void calc() override;
};

class SeniorManager : public ProjectManager
{
public:
	SeniorManager();
	SeniorManager(int id, std::string name);
	void calc() override;
	void setSubs(int n);
	void setProjects(std::vector<Proj> &p);
protected:
	int sub;
	std::vector<Proj> projects;
};