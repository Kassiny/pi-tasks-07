#pragma once

#include "Employee.h"
#include "Project.h"
#include "Worktime.h"
#include "Heading.h"
#include <vector>

using namespace std;


class  Engineer: public Employee, public Worktime, public Project
{
public:
	Engineer();
	Engineer(int id, string name);
	Engineer(int id, string name, double rateH, double rateP);
	void calc() override;
	double premiumhour() override;
	double premiumProject() override;
	virtual void setProject(Proj *p);
protected:
	double rateHour;
	double rateProject;
	string projectName;
	int worktime;
	Proj* project;
};

class Programmer: public Engineer
{
public:
	Programmer();
	Programmer(int id, string name);
	void doWork() override;
	void doWork(int days) override;
	void  setProject(Proj *p) override;

protected:
};
class Tester : public Engineer
{
public:
	Tester();
	Tester(int id, string name);
	void doWork() override;
	void doWork(int days) override;
	void setProject(Proj *p) override;
};

class TeamLeader : public Programmer, public Heading
{
public:
	TeamLeader();
	TeamLeader(int id, string name);
	double premiumHeading() override;
	void calc() override;
};

