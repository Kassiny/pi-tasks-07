#include <iostream>
#include <fstream>
#include "Management.h"
#include "Engineer.h"
#include "Personal.h"
#include "Project.h"


using namespace std;

void loadProjects(vector<Proj> &p, string path)
{
	ifstream in(path);
	int num;
	in >> num;
	for (int i = 0; i < num; i++)
	{
		Proj proj;
		in >> proj.name >> proj.budget;
		p.push_back(proj);
	}
}

void loadWorkers(vector<Employee> &workers, vector<Proj> &projects, string path)
{


}

int main()
{
	vector<Employee*> workers(0);
	vector<Proj> projects;
	loadProjects(projects, "projects.txt");
	ifstream in("workers.txt");
	int num, id;
	string im, fam, otch, fio;
	string pos;
	in >> num;
	for (int i = 0; i < num; i++)
	{
		in >> id >> im >> fam >> otch;
		fio = im + " " + fam + " " + otch;
		in >> pos;
		if (pos == "Cleaner")
		{
			Cleaner* cl = new Cleaner(id, fio);
			cl->doWork(22);
			workers.push_back(cl);
		}
		else if (pos == "Driver")
		{
			Driver* dr = new Driver(id, fio);
			dr->doWork(22);
			workers.push_back(dr);
		}
		else if (pos == "Tester")
		{
			Tester* t = new Tester(id, fio);
			t->doWork(22);
			string projName;
			in >> projName;
			for (int j = 0; j < projects.size(); j++)
			{
				if (projects[j].name == projName)
				{
					t->setProject(&projects[j]);
				}
			}
			workers.push_back(t);
		}
		else if (pos == "Programmer")
		{
			Programmer* p = new Programmer(id, fio);
			p->doWork(22);
			string projName;
			in >> projName;
			for (int j = 0; j < projects.size(); j++)
			{
				if (projects[j].name == projName)
				{
					p->setProject(&projects[j]);
				}
			}
			workers.push_back(p);
		}
		else if (pos == "TeamLeader")
		{
			TeamLeader* t = new TeamLeader(id, fio);
			t->doWork(22);
			string projName;
			in >> projName;
			for (int j = 0; j < projects.size(); j++)
			{
				if (projects[j].name == projName)
				{
					t->setProject(&projects[j]);
				}
			}
			workers.push_back(t);
		}
		else if (pos == "Manager")
		{
			Manager* m = new Manager(id, fio);
			string projName;
			in >> projName;
			for (int j = 0; j < projects.size(); j++)
			{
				if (projects[j].name == projName)
				{
					m->setProject(&projects[j]);
				}
			}
			workers.push_back(m);
		}
		else if (pos == "ProjectManager")
		{
			ProjectManager *pm = new ProjectManager(id, fio);
			string projName;
			in >> projName;
			for (int j = 0; j < projects.size(); j++)
			{
				if (projects[j].name == projName)
				{
					pm->setProject(&projects[j]);
				}
			}
			workers.push_back(pm);

		}
		else if (pos == "SeniorManager")
		{
			SeniorManager *sm = new SeniorManager(id, fio);
			sm->setProjects(projects);
			workers.push_back(sm);
		}
	}
	for (int i = 0; i < workers.size(); i++)
	{
		workers[i]->calc();
	}
	ofstream payments("payments.txt");
	for (int i = 0; i < workers.size(); i++)
	{
		payments << workers[i]->getId() << " " << workers[i]->getName() <<\
			" payment: " << workers[i]->getPayment() << endl;
	}
	return 0;
}