#pragma once

#include <string>


class  Employee
{
public:

	virtual void calc() = 0;

	double getPayment()
	{ 
		return payment;
	}

	int getId()
	{
		return id;
	}

	std::string getName()
	{
		return name;
	}

	Employee(int id, std::string name):payment(0)
	{

		this->id = id;
		this->name = name;
	}
	 Employee()
	 {}
protected:
	int id;
	double payment;
	std::string name;
};


